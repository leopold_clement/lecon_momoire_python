all : presentation.pdf handout.pdf note.pdf
	echo "done"

handout.pdf : lecon.tex handout.tex handout.bib figure
	lualatex -shell-escape handout.tex
	bibtex handout 
	lualatex -shell-escape handout.tex
	lualatex -shell-escape handout.tex

presentation.pdf : lecon.tex presentation.tex presentation.bib figure
	lualatex -shell-escape presentation.tex
	bibtex presentation
	lualatex -shell-escape presentation.tex
	lualatex -shell-escape presentation.tex

note.pdf : lecon.tex note.tex note.bib figure
	lualatex -shell-escape note.tex
	bibtex note
	lualatex -shell-escape note.tex
	lualatex -shell-escape note.tex

presentation.bib : biblio.bib
	cp biblio.bib presentation.bib

handout.bib : biblio.bib
	cp biblio.bib handout.bib

note.bib : biblio.bib
	cp biblio.bib note.bib

clean : clean-handout clean-presentation clean-note
	rm -f handout.pdf
	rm -f presentation.pdf
	rm -f note.pdf
	rm -f *.pgf

clean-handout :
	rm -f handout.aux
	rm -f handout.bbl
	rm -f handout.bib
	rm -f handout.blg
	rm -f handout.out
	rm -f handout.log
	rm -f handout.toc
	rm -f handout.lof
	rm -f handout.lot
	rm -f handout.lol
	rm -f handout.ist
	rm -f handout.glg
	rm -f handout.gls
	rm -f handout.glo
	rm -f handout.nav
	rm -f handout.snm
	rm -f handout.vrb
	rm -f texput.log
	rm -f -r _minted-handout

clean-presentation :
	rm -f presentation.aux
	rm -f presentation.bbl
	rm -f presentation.bib
	rm -f presentation.blg
	rm -f presentation.out
	rm -f presentation.log
	rm -f presentation.toc
	rm -f presentation.lof
	rm -f presentation.lot
	rm -f presentation.lol
	rm -f presentation.ist
	rm -f presentation.glg
	rm -f presentation.gls
	rm -f presentation.glo
	rm -f presentation.nav
	rm -f presentation.snm
	rm -f presentation.vrb
	rm -f texput.log
	rm -f -r _minted-presentation

clean-note :
	rm -f note.aux
	rm -f note.bbl
	rm -f note.bib
	rm -f note.blg
	rm -f note.out
	rm -f note.log
	rm -f note.toc
	rm -f note.lof
	rm -f note.lot
	rm -f note.lol
	rm -f note.ist
	rm -f note.glg
	rm -f note.gls
	rm -f note.glo
	rm -f note.nav
	rm -f note.snm
	rm -f note.vrb
	rm -f texput.log
	rm -f -r _minted-note

figure : 
	echo plop
